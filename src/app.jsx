import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ImageUploader from './ImageUploader.jsx';

function render() {
  ReactDOM.render(<ImageUploader/>, document.body);
}

render();